# Listerr

Un gestor de tareas con backend y api para el proyecto final de curso.

## Requisitos previos  a la instalación del proyecto

- Node.js (se recomienda nvm para una instalación rápida y sencilla).
- MySQL o MariaDB (el nuevo método de autenticación en las versiones más recientes de MySQL no permitirá el acceso a la app).

## Pasos a seguir para la instalación del proyecto

- Descargue o clone los archivos de mi repositorio: <https://gitlab.com/ComputerGremlin/Listerr>
- Importe la base de datos de la aplicación usando el archivo Listerr.sql (importar este archivo creará la base de datos si esta no existe). En caso de fallo puede ser que esté usando una versión reciente de MySQL con su nuevo sistema de autenticación. Para cambiar a la autenticación tradicional ejecute este comando:

```sql
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'new-password';
```

- Acceda a server/models/db.js e inserte su usuario y contraseña.
- Abra una terminal en el directorio raíz y ejecute `npm install`, esto instalará las dependensias de node del servidor.
- Cuando la operación acabe ejecute `npm run build`, esto instalará las dependencias para el desarrollo y la compilación de la aplicación de React y compilara una versión de producción que es la que serviremos con node.
- Una vez finalizado ejecute `npm start` para iniciar el servido de la aplicación y de la API.
- Acceda a la aplicación desde su ordenador usando la dirección y puerto: <http://localhost:3001/>
