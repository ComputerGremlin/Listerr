const sql = require('./db.js');

// constructor
const User = function (user) {
    this.name = user.name;
    this.username = user.username;
    this.email = user.email;
    this.password = user.password;
};

User.create = (newUser, result) => {
    sql.query('INSERT INTO users SET ?', newUser, (err, res) => {
        if (err) {
            console.log('error: ', err);
            result(err, null);
            return;
        }
        console.log('created user: ', { id: res.insertId, ...newUser });
        result(null, { id: res.insertId, ...newUser });
    });
};

User.findById = (id, result) => {
    sql.query('SELECT * FROM users WHERE id = ?', [id], (err, res) => {
        if (err) {
            console.log('error: ', err);
            result(err, null);
            return;
        }
        if (res.length) {
            console.log('found user: ', res[0]);
            result(null, res[0]);
            return;
        }
        // not found User with the id
        result({ kind: 'not_found' }, null);
    });
};

User.findByUsernameAndPassword = (username, password, result) => {
    sql.query(
        `
            SELECT * FROM users 
            WHERE username = ?
                AND password = ?
        `, 
        [username, password],
        (err, res) => {
            if (err) {
                console.log('error: ', err);
                result(err, null);
                return;
            }
            if (res.length) {
                console.log('found user: ', res[0]);
                result(null, res[0]);
                return;
            }
            // not found User with the id
            result({ kind: 'not_found' }, null);
        }
    );
};

User.findByUsername = (username, result) => {
    sql.query(
        `
            SELECT * FROM users 
            WHERE username = ?
        `, 
        [username],
        (err, res) => {
            if (err) {
                console.log('error: ', err);
                result(err, null);
                return;
            }
            if (res.length) {
                console.log('found user: ', res[0]);
                result(null, res[0]);
                return;
            }
            // not found User with the id
            result({ kind: 'not_found' }, null);
        }
    );
};

User.getAll = (username, result) => {
    let query = 'SELECT * FROM users';
    if (username) {
        query += ' WHERE username LIKE ?';
        username = '%' + username + '%';
    }
    sql.query(query, [username], (err, res) => {
        if (err) {
            console.log('error: ', err);
            result(null, err);
            return;
        }
        console.log('users: ', res);
        result(null, res);
    });
};

User.updateById = (id, user, result) => {
    sql.query(
        'UPDATE users SET name = ?, username = ?, email = ?, password = ? WHERE id = ?',
        [user.name, user.username, user.email, user.password, id],
        (err, res) => {
            if (err) {
                console.log('error: ', err);
                result(null, err);
                return;
            }
            if (res.affectedRows == 0) {
                // not found User with the id
                result({ kind: 'not_found' }, null);
                return;
            }
            console.log('updated user: ', { id: id, ...user });
            result(null, { id: id, ...user });
        }
    );
};

User.remove = (id, result) => {
    sql.query('DELETE FROM users WHERE id = ?', id, (err, res) => {
        if (err) {
            console.log('error: ', err);
            result(null, err);
            return;
        }
        if (res.affectedRows == 0) {
            // not found User with the id
            result({ kind: 'not_found' }, null);
            return;
        }
        console.log('deleted user with id: ', id);
        result(null, res);
    });
};
module.exports = User;
