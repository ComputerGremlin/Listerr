const sql = require('./db.js');

// constructor
const List = function (list) {
    this.name = list.name;
    this.user = list.user;
    this.listgroup = list.listgroup;
};

List.create = (newList, result) => {
    // console.log(newList);
    sql.query('INSERT INTO lists SET ?', newList, (err, res) => {
        if (err) {
            console.log('error: ', err);
            result(err, null);
            return;
        }
        // console.log('created list: ', { id: res.insertId, ...newList });
        result(null, { id: res.insertId, ...newList });
    });
};

List.findByUserId = (id, result) => {
    let query = `
        SELECT 
            l.id,
            l.name,
            l.user AS ownerUserID,
            e.id AS itemID,
            e.name AS text,
            e.info,
            e.state,
            s.id AS sharedUserID,
            u.id AS userID,
            u.username
        FROM lists l
            LEFT JOIN listelments e ON l.id = e.list
            LEFT JOIN shared s ON s.id_list = l.id
            LEFT JOIN users u ON u.id = s.id_user
        WHERE l.user = ? OR s.id_user = ?
    `;
    sql.query(query, [id, id], (err, res) => {
        if (err) {
            console.log('error: ', err);
            result(err, null);
            return;
        }
        // console.log('lists: ', res);
        result(null, res);
    });
};

List.getAll = (result) => {
    let query = 'SELECT * FROM lists';
    sql.query(query, (err, res) => {
        if (err) {
            console.log('error: ', err);
            result(null, err);
            return;
        }
        // console.log('lists: ', res);
        result(null, res);
    });
};

List.updateById = (id, list, result) => {
    sql.query(`
        UPDATE lists
        SET
            name = ?,
            user = ?,
            listgroup = ?
        WHERE 
            id = ?
        `,
        [
            list.name, 
            list.user, 
            list.listgroup, 
            id
        ],
        (err, res) => {
            if (err) {
                console.log('error: ', err);
                result(null, err);
                return;
            }
            if (res.affectedRows == 0) {
                // not found List with the id
                result({ kind: 'not_found' }, null);
                return;
            }
            // console.log('updated list: ', { id: id, ...list });
            result(null, { id: id, ...list });
        }
    );
};

List.remove = (id, result) => {
    sql.query('DELETE FROM lists WHERE id = ?', id, (err, res) => {
        if (err) {
            console.log('error: ', err);
            result(null, err);
            return;
        }
        if (res.affectedRows == 0) {
            // not found List with the id
            result({ kind: 'not_found' }, null);
            return;
        }
        // console.log('deleted list with id: ', id);
        result(null, res);
    });
};

module.exports = List;
