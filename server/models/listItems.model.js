const sql = require('./db.js');

// constructor
const ListItem = function (listItem) {
    this.name = listItem.name;
    this.info = listItem.info;
    this.state = listItem.state;
    this.endDate = listItem.endDate;
    this.list = listItem.list;
};

ListItem.create = (newListItem, result) => {
    // console.log(newListItem);
    sql.query('INSERT INTO listelments SET ?', newListItem, (err, res) => {
        if (err) {
            console.log('error: ', err);
            result(err, null);
            return;
        }
        // console.log('created listItem: ', { id: res.insertId, ...newListItem });
        result(null, { id: res.insertId, ...newListItem });
    });
};

// find by list
ListItem.findByListId = (id, result) => {
    sql.query('SELECT * FROM listelments WHERE list = ?', [id], (err, res) => {
        if (err) {
            console.log('error: ', err);
            result(err, null);
            return;
        }
        // console.log('found listItem: ', res);
        result(null, res);
    });
};

ListItem.getAll = (result) => {
    let query = 'SELECT * FROM listelments';
    sql.query(query, (err, res) => {
        if (err) {
            console.log('error: ', err);
            result(null, err);
            return;
        }
        // console.log('listelments: ', res);
        result(null, res);
    });
};

ListItem.updateById = (id, listItem, result) => {
    sql.query(
        'UPDATE listelments SET name = ?, info = ?, state = ?, endDate = ?, list = ? WHERE id = ?',
        [listItem.name, listItem.info, listItem.state, listItem.endDate, listItem.list, id],
        (err, res) => {
            if (err) {
                console.log('error: ', err);
                result(null, err);
                return;
            }
            if (res.affectedRows == 0) {
                // not found ListItem with the id
                result({ kind: 'not_found' }, null);
                return;
            }
            // console.log('updated listItem: ', { id: id, ...listItem });
            result(null, { id: id, ...listItem });
        }
    );
};

ListItem.remove = (id, result) => {
    sql.query('DELETE FROM listelments WHERE id = ?', id, (err, res) => {
        if (err) {
            console.log('error: ', err);
            result(null, err);
            return;
        }
        if (res.affectedRows == 0) {
            // not found ListItem with the id
            result({ kind: 'not_found' }, null);
            return;
        }
        // console.log('deleted listItem with id: ', id);
        result(null, res);
    });
};
module.exports = ListItem;
