const mysql = require('mysql');
// Creación de una conexión a la base de datos
const connection = mysql.createConnection({
    host: 'localhost',
    database: 'listerr',
    user: 'root',
    password: ''
});
module.exports = connection;
