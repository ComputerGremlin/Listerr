const sql = require('./db.js');

// constructor
const SharedUser = function (sharedUser) {
    this.id_user = sharedUser.id_user;
    this.id_list = sharedUser.id_list;
    this.id_group = sharedUser.id_group;
};

SharedUser.create = (newSharedUser, result) => {
    // console.log(newSharedUser);
    sql.query('INSERT INTO shared SET ?', newSharedUser, (err, res) => {
        if (err) {
            console.log('error: ', err);
            result(err, null);
            return;
        }
        result(null, { id: res.insertId, ...newSharedUser });
    });
};

SharedUser.remove = (id, result) => {
    sql.query('DELETE FROM shared WHERE id = ?', id, (err, res) => {
        if (err) {
            console.log('error: ', err);
            result(null, err);
            return;
        }
        if (res.affectedRows == 0) {
            // not found SharedUser with the id
            result({ kind: 'not_found' }, null);
            return;
        }
        result(null, res);
    });
};

module.exports = SharedUser;
