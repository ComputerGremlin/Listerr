module.exports = app => {
    const login = require('../controllers/login.controller');
    const users = require('../controllers/users.controller');
    const lists = require('../controllers/lists.controller');
    const listItems = require('../controllers/listItems.controller');
    const sharedUsers = require('../controllers/sharedUsers.controller');
    var router = require('express').Router();

    app.post('/login', login.auth);
    app.post('/logout', login.bye);
    
    app.all('/api/*', (req, res, next) => {
        if (!req.session.loggedin) {
            res.status(401).send({
                message: 'You have to be logged in'
            });
        } else {
            next();
        }

    });

    // Create a new User
    // router.post('/users', users.create);
    app.post('/createUser', users.create);
    // Retrieve all Users, also get User by username with HTTP GET method
    router.get('/users', users.findAll);
    // Retrieve a single User with id
    router.get('/users/:id', users.findOne);
    // Update a User with id
    router.put('/users/:id', users.update);
    // Delete a User with id
    router.delete('/users/:id', users.delete);

    // Create a new list
    router.post('/lists', lists.create);
    // Find all lists
    router.get('/lists', lists.findAll);
    // Find all lists from a user
    router.get('/lists/:id', lists.findByUser);
    // Update a list
    router.put('/lists/:id', lists.update);
    // delete a list
    router.delete('/lists/:id', lists.delete);
    // check/uncheck all list items
    router.patch('/lists/checkall/:id', lists.checkAll);
    // empty list contents
    router.delete('/lists/empty/:id', lists.empty);

    // Create a new Shared user
    router.post('/sharedusers', sharedUsers.create);
    router.delete('/sharedusers/:id', sharedUsers.delete);

    // Create a new list element
    router.post('/listitems', listItems.create);
    // Find all list elements
    router.get('/listitems', listItems.findAll);
    // Find all lists elements from a list
    router.get('/listitems/:id', listItems.findByList);
    // Update a list element
    router.put('/listitems/:id', listItems.update);
    // delete a list element
    router.delete('/listitems/:id', listItems.delete);

    app.use('/api', router);
};
