const List = require('../models/lists.model.js');
const ListItem = require('../models/listItems.model');

// Create and Save a new List
exports.create = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: 'Content can not be empty!'
        });
    }

    const list = new List({
        name: req.body.name,
        user: req.body.user,
        listgroup: req.body.listgroup || null
    });

    List.create(list, (err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || 'Some error occurred while creating the List.'
            });
        else res.send(data);
    });
};

exports.findAll = (req, res) => {
    List.getAll((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || 'Some error occurred while retrieving lists.'
            });
        else res.send(data);
    });
};

// Find all Lists identified by the user id in the request
exports.findByUser = (req, res) => {
    List.findByUserId(req.params.id, (err, data) => {
        if (err) {
            if (err.kind === 'not_found') {
                res.status(404).send({
                    message: `Not found List with user ${req.params.id}.`
                });
            } else {
                res.status(500).send({
                    message: `Error retrieving List with user ${req.params.id}.`
                });
            }
        } else {
            const listArray = [];
            const insertedIDs = [];
            let actualItem = -1;

            const lists = {};

            for (const listRow of data) {
                // if(insertedIDs.includes(listQuery.id)) {
                //     // insert only element data
                //     listArray[actualItem].list.push({
                //         id: listQuery.itemID,
                //         text: listQuery.text,
                //         description: listQuery.info,
                //         checked: listQuery.state,
                //         shared: {}
                //     });
                // } else {
                //     // insert the full list and the id in insertedIDs and update actualItem
                //     listArray.push({
                //         id: listQuery.id,
                //         name: listQuery.name,
                //         list: listQuery.text 
                //             ? [{
                //                 id: listQuery.itemID,
                //                 text: listQuery.text,
                //                 description: listQuery.info,
                //                 checked: listQuery.state
                //             }] 
                //             : []
                //     });
                //     insertedIDs.push(listQuery.id);
                //     actualItem++;
                // }

                if (!lists.hasOwnProperty(listRow.id)) {
                    lists[listRow.id] = {
                        id: listRow.id,
                        name: listRow.name,
                        ownerUserID: listRow.ownerUserID,
                        list: {},
                        shared: {}
                    };
                }

                var currentList = lists[listRow.id];

                if (listRow.itemID && !currentList.list.hasOwnProperty(listRow.itemID)) {
                    currentList.list[listRow.itemID] = {
                        id: listRow.itemID,
                        text: listRow.text,
                        description: listRow.info,
                        checked: listRow.state
                    };
                }

                if (listRow.userID && !currentList.shared.hasOwnProperty(listRow.userID)) {
                    currentList.shared[listRow.userID] = {
                        id: listRow.sharedUserID,
                        userID: listRow.userID,
                        username: listRow.username
                    };
                }
            }

            for (const listID in lists) {
                lists[listID].list = Object.values(lists[listID].list);
                lists[listID].shared = Object.values(lists[listID].shared);
            }

            res.send(Object.values(lists));
        }
    });
};

// Update a List identified by the id in the request
exports.update = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: 'Content can not be empty!'
        });
    }
    // console.log(req.body);

    List.updateById(
        req.params.id,
        new List(req.body),
        (err, data) => {
            if (err) {
                if (err.kind === 'not_found') {
                    res.status(404).send({
                        message: `Not found List with id ${req.params.id}.`
                    });
                } else {
                    res.status(500).send({
                        message: `Error updating List with id ${req.params.id}.`
                    });
                }
            } else res.send(data);
        }
    );
};

exports.checkAll = (req, res) => {
    ListItem.findByListId(req.params.id, (err, data) => {
        if (err) {
            if (err.kind === 'not_found') {
                res.status(404).send({
                    message: `Not found List with id ${req.params.id}.`
                });
            } else {
                res.status(500).send({
                    message: 'Error retrieving ListItems of List with id ' + req.params.id
                });
            }
        } else{
            let setValue = false;
        
            for (const itemToCheck of data) {
                if (!itemToCheck.state) {
                    setValue = true;
                }
            }

            for (const itemToEdit of data) {
                itemToEdit.state = setValue;
                ListItem.updateById(itemToEdit.id, itemToEdit, () => {});
            }
            res.send({message: 'ok'});
        }
    });
};

// Delete a List with the specified id in the request
exports.delete = (req, res) => {
    List.remove(req.params.id, (err, data) => {
        if (err) {
            if (err.kind === 'not_found') {
                res.status(404).send({
                    message: `Not found List with id ${req.params.id}.`
                });
            } else {
                res.status(500).send({
                    message: 'Could not delete List with id ' + req.params.id
                });
            }
        } else res.send({ message: `List was deleted successfully!` });
    });
};

exports.empty = (req, res) => {
    ListItem.findByListId(req.params.id, (err, data) => {
        if (err) {
            if (err.kind === 'not_found') {
                res.status(404).send({
                    message: `Not found List with id ${req.params.id}.`
                });
            } else {
                res.status(500).send({
                    message: 'Error removing ListItems of List with id ' + req.params.id
                });
            }
        } else{
            for (const itemToEdit of data) {
                ListItem.remove(itemToEdit.id, () => {});
            }
            res.send({message: 'ok'});
        }
    });
};
