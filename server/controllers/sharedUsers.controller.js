const User = require('../models/users.model.js');
const SharedUser = require('../models/sharedUsers.model.js');

// Create and Save a new User
exports.create = (req, res) => {
    // Validate request
    if (!req.body) {
        res.status(400).send({
            message: 'Content can not be empty!'
        });
        return;
    }

    if (req.session.username == req.body.username) {
        res.status(400).send({
            message: 'Cannot share to yourself!'
        });
        return;
    }
    
    // look for user by username
    User.findByUsername(req.body.username, (err, data) => {
        if (err) {
            if (err.kind === 'not_found') {
                res.status(404).send({
                    message: `Not found user with username ${req.body.username}.`
                });
            } else {
                res.status(500).send({
                    message: `Error retrieving user with username ${req.body.username}.`
                });
            }
        } else {
            // data = user
            const sharedUser = new SharedUser({
                id_user: data.id,
                id_list: req.body.id
            });

            SharedUser.create(sharedUser, (err, data) => {
                if (err)
                    res.status(500).send({
                        message:
                            err.message || 'Some error occurred while creating the relation between user and list.'
                    });
                else res.send(data);
            });
        }
    });
};

// Delete a User with the specified id in the request
exports.delete = (req, res) => {
    SharedUser.remove(req.params.id, (err, data) => {
        if (err) {
            if (err.kind === 'not_found') {
                res.status(404).send({
                    message: `Not found shared user with id ${req.params.id}.`
                });
            } else {
                res.status(500).send({
                    message: 'Could not delete shared user with id ' + req.params.id
                });
            }
        } else res.send({ message: `User to list relation was deleted successfully!` });
    });
};
