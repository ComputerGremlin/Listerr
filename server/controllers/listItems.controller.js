const ListItem = require('../models/listItems.model');

// Create and Save a new ListItem
exports.create = (req, res) => {
    // Validate request
    if (!req.body) {
        res.status(400).send({
            message: 'Content can not be empty!'
        });
    }
    // Crea un listItem
    const listItem = new ListItem({
        name: req.body.name,
        info: req.body.info || null,
        state: req.body.state || false,
        endDate: req.body.endDate || null,
        list: req.body.list
    });
    // Save ListItem in the database
    ListItem.create(listItem, (err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || 'Some error occurred while creating the ListItem.'
            });
        else res.send(data);
    });
};

exports.findAll = (req, res) => {
    ListItem.getAll((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || 'Some error occurred while retrieving listItems.'
            });
        else res.send(data);
    });
};

// Encuentra ListItems pertenecientes a una lista
exports.findByList = (req, res) => {
    ListItem.findByListId(req.params.id, (err, data) => {
        if (err) {
            if (err.kind === 'not_found') {
                res.status(404).send({
                    message: `Not found List with id ${req.params.id}.`
                });
            } else {
                res.status(500).send({
                    message: 'Error retrieving ListItems of List with id ' + req.params.id
                });
            }
        } else res.send(data);
    });
};

// Update a ListItem identified by the id in the request
exports.update = (req, res) => {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: 'Content can not be empty!'
        });
    }
    // console.log(req.body);
    ListItem.updateById(
        req.params.id,
        new ListItem(req.body),
        (err, data) => {
            if (err) {
                if (err.kind === 'not_found') {
                    res.status(404).send({
                        message: `Not found ListItem with id ${req.params.id}.`
                    });
                } else {
                    res.status(500).send({
                        message: 'Error updating ListItem with id ' + req.params.id
                    });
                }
            } else res.send(data);
        }
    );
};

// Delete a ListItem with the specified id in the request
exports.delete = (req, res) => {
    ListItem.remove(req.params.id, (err, data) => {
        if (err) {
            if (err.kind === 'not_found') {
                res.status(404).send({
                    message: `Not found ListItem with id ${req.params.id}.`
                });
            } else {
                res.status(500).send({
                    message: 'Could not delete ListItem with id ' + req.params.id
                });
            }
        } else res.send({ message: `ListItem was deleted successfully!` });
    });
};
