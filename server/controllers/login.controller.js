const User = require('../models/users.model.js');

// Find a single User with username and password
exports.auth = (req, res) => {
    User.findByUsernameAndPassword(
        req.body.username, 
        req.body.password, 
        (err, data) => {
            if (err) {
                if (err.kind === 'not_found') {
                    res.status(404).send({
                        message: `Not found User with that username or password is not right.`
                    });
                } else {
                    res.status(500).send({
                        message: 'Error retrieving User'
                    });
                }
            } else {
                req.session.loggedin = true;
				req.session.username = req.body.username;

                res.send({
                    token: 'thisIsNotASecureToken',
                    user: data
                });
            };
        }
    );
};

exports.bye = (req, res) => {
    req.session.destroy((err) => {
        res.redirect('/') // will always fire after session is destroyed
    });
};