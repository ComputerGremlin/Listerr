const path = require('path');
const express = require('express');
const session = require('express-session');
const app = express();
const PORT = process.env.PORT || 3001;

app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));

// Parseo de peticiones de tipo json
app.use(express.json());
    
// Servido de los archivos de la aplicación cliente
app.use(express.static(path.resolve(__dirname, '../client/build')));

// Rutas para servir la API
require('./routes/routes')(app);

// Todas las peticiones GET no manejadas anteriormente devolverán la aplicación cliente
app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../client/build', 'index.html'));
});

// Escucha de peticiones
app.listen(PORT, () => {
    console.log(`Server listening on ${PORT}`);
});
