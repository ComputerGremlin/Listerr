import { useState } from 'react';
import PropTypes from 'prop-types';
import ToastAlert from './ToastAlert';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

export default function Login(props) {
    const [username, setUserName] = useState();
    const [password, setPassword] = useState();
    const [name, setName] = useState();
    const [email, setEmail] = useState();
    const [toastBody, setToastBody] = useState('');
    const [toastBackground, setToastBackground] = useState('');
    
    async function loginUser(credentials) {
        return fetch('/login/', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(credentials)
        }).then((response) => {
            if (!response.ok) {
                if (response.status === 404) {
                    setToastBody('Usuario y/o contraseña no válido/s.');
                } else {
                    setToastBody('Error creando usuario');
                }
                throw new Error(`HTTP error! Status: ${response.status}`);
            }
    
            return response.json();
        })
    }

    const handleSubmit = async e => {
        e.preventDefault();
        const token = await loginUser({
            username,
            password
        });

        props.setUser(token.user.id);
        localStorage.actualUser = token.user.id;
        props.setToken(token);
    }

    const handleSignIn = e => {
        e.preventDefault();
        const userData = {
            username,
            password,
            name,
            email
        };

        fetch('/createUser/', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(userData)
        }).then((response) => {
            if (response.ok) {
                setToastBackground('success');
                setToastBody('Usuario creado con éxito');
                setTimeout(() => {
                    window.location.assign('/');
                }, 3000);
            } else {
                setToastBackground('warning');
                setToastBody('Error creando usuario');
            }
        })
    }

    return(
        <BrowserRouter>
            <Routes>
                <Route path='/' 
                    element={
                        <>
                            <div className='login-form' style={{margin: '50px auto'}}>
                                <form onSubmit={handleSubmit}>
                                    <h2 className='text-center '>Log in</h2>
                                    <div className="form-floating mt-3">
                                        <input 
                                            type='text' 
                                            className='form-control' 
                                            placeholder='Username' 
                                            required='required' 
                                            onChange={e => setUserName(e.target.value)}
                                        />
                                        <label htmlFor="password">Username</label>
                                    </div>
                                    <div className="form-floating mt-3">
                                        <input 
                                            type='password' 
                                            id='password' 
                                            className='form-control' 
                                            placeholder='Password' 
                                            required='required'
                                            onChange={e => setPassword(e.target.value)}
                                        />
                                        <label htmlFor="password">Password</label>
                                    </div>
                                    <div className='form-group mt-3'>
                                        <button 
                                            type='submit' 
                                            className='btn btn-primary btn-block' 
                                            style={{width: '100%'}}
                                        >
                                            Log in
                                        </button>
                                    </div>
                                </form>
                                <p className='text-center'><a href='/sign-in'>Create an Account</a></p>
                            </div>
                            <ToastAlert 
                                toastBody={toastBody}
                                setToastBody={setToastBody}
                                toastBackground='warning'
                            />
                        </>
                    }>
                </Route>
                <Route path='/sign-in' 
                    element={
                        <>
                            <div className='login-form' style={{margin: '50px auto'}}>
                                <form onSubmit={handleSignIn}>
                                    <h2 className='text-center'>Sign in</h2>
                                    <div className="form-floating mt-3">
                                        <input 
                                            type='text' 
                                            id="name"
                                            className='form-control' 
                                            placeholder='Name' 
                                            required='required' 
                                            onChange={e => setName(e.target.value)}
                                        />
                                        <label htmlFor="name">Name</label>
                                    </div>
                                    <div className='form-floating mt-3'>
                                        <input 
                                            type='text' 
                                            id="username"
                                            className='form-control' 
                                            placeholder='Username' 
                                            required='required' 
                                            onChange={e => setUserName(e.target.value)}
                                        />
                                        
                                        <label htmlFor="username">Username</label>
                                    </div>
                                    <div className='form-floating mt-3'>
                                        <input 
                                            type='text' 
                                            id="email"
                                            className='form-control' 
                                            placeholder='Email' 
                                            required='required' 
                                            onChange={e => setEmail(e.target.value)}
                                        />
                                        
                                        <label htmlFor="email">Email</label>
                                    </div>
                                    <div className='form-floating mt-3'>
                                        <input 
                                            type='password' 
                                            className='form-control' 
                                            placeholder='Password' 
                                            required='required'
                                            onChange={e => setPassword(e.target.value)}
                                        />
                                        
                                        <label htmlFor="password">Password</label>
                                    </div>
                                    <div className='form-group mt-3'>
                                        <button 
                                            type='submit' 
                                            className='btn btn-primary btn-block' 
                                            style={{width: '100%'}}
                                        >
                                            Sign in
                                        </button>
                                    </div>
                                </form>
                                <p className='text-center'><a href='/'>Return</a></p>
                            </div>
                            <ToastAlert 
                                toastBody={toastBody}
                                setToastBody={setToastBody}
                                toastBackground={toastBackground}
                            />
                        </>
                    }>
                </Route>
            </Routes>
        </BrowserRouter>
    );
}

Login.propTypes = {
    setToken: PropTypes.func.isRequired
}