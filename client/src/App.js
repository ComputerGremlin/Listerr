import { useEffect, useState } from 'react';
import useToken from './useToken';
import Login from './Login';
import List from './List';
import SideMenu from './SideMenu';
import { Alert } from 'react-bootstrap';
import InputModal from './InputModal';
import ToastAlert from './ToastAlert';

const App = () => {
    const { token, setToken } = useToken();
    const [toastBody, setToastBody] = useState('');
    const [showNewListModal, setShowNewListModal] = useState(false);
    const [selectedList, setSelectedList] = useState(localStorage.selectedList
        ? parseInt(localStorage.selectedList) 
        : null
    );
    const [myLists, setMyLists] = useState([]);
    const [getDBDataTrigger, setGetDBDataTrigger] = useState(false);
    const [user, setUser] = useState(localStorage.actualUser
        ? parseInt(localStorage.actualUser)
        : null
    );

    useEffect(() => {
        fetch(`/api/lists/${user}`)
        .then((res) => {
            if (!res.ok) {
                setToken(null);
                return [];
            } else {
                return res.json();
            }
        })
        .then((data) => setMyLists(data));
    }, [getDBDataTrigger, token]);
    
    if (!token) {
        return (
            <Login 
                setToken={setToken} 
                setUser={setUser} 
                setToastBody={setToastBody}
            />
        );
    }

    const logOut = () => {
        fetch('/logout/', { method: 'POST' });
        localStorage.clear();
        window.location.assign('/');
    };
    
    const selectList = (listIndex) => {
        setSelectedList(listIndex);
        localStorage.selectedList = listIndex;
    };

    const displayedList = () => {
        let returnedList = null;
        myLists.forEach((list) => {
            if (list.id === selectedList) {
                returnedList = list;
            }
        });
        if (returnedList === null && myLists[0]) {
            setSelectedList(myLists[0].id);
        };
        return returnedList;
    };

    const createList = (inputValues) => {
        if (!inputValues.nombre || !inputValues.nombre.trim()) {
            setToastBody('Inserta un nombre válido');
            return 'invalid name';
        }
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                name: inputValues.nombre,
                user: user,
                listgroup: null
            })
        };
        fetch('/api/lists/', requestOptions);
        setGetDBDataTrigger(!getDBDataTrigger);
    };

    const renameList = (inputValues) => {
        const requestOptions = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                name: inputValues.name,
                user: user,
                listgroup: null
            })
        };
        fetch(`/api/lists/${inputValues.id}`, requestOptions);
        setGetDBDataTrigger(!getDBDataTrigger);
    };

    const deleteList = (id) => {
        fetch(`/api/lists/${id}`, { method: 'DELETE' });
        setGetDBDataTrigger(!getDBDataTrigger);
    };

    const checkAll = (id) => {
        fetch(`/api/lists/checkall/${id}`, { method: 'PATCH' });
        setGetDBDataTrigger(!getDBDataTrigger);
    }

    const clearList = (id) => {
        fetch(`/api/lists/empty/${id}`, { method: 'DELETE' });
        setGetDBDataTrigger(!getDBDataTrigger);
    };

    const createListItem = (newListItem) => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(newListItem)
        };
        fetch('/api/listitems/', requestOptions);
        setGetDBDataTrigger(!getDBDataTrigger);
    };

    const editListItem = (listItem) => {
        const requestOptions = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(listItem)
        };
        fetch(`/api/listitems/${listItem.id}`, requestOptions);
        setGetDBDataTrigger(!getDBDataTrigger);
    };

    const deleteListItem = (id) => {
        fetch(`/api/listitems/${id}`, { method: 'DELETE' });
        setGetDBDataTrigger(!getDBDataTrigger);
    };

    const addSharedUser = (newSharedUserItem) => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(newSharedUserItem)
        };
        fetch('/api/sharedusers/', requestOptions)
        .then((response) => {
            if (response.status === 404) {
                setToastBody('User not found');
            }
        });
        setGetDBDataTrigger(!getDBDataTrigger);
    };

    const deleteSharedUser = (id) => {
        fetch(`/api/sharedusers/${id}`, { method: 'DELETE' });
        setGetDBDataTrigger(!getDBDataTrigger);
    };

    return (
        <>
            <SideMenu
                myLists={myLists}
                user={user}
                selectedList={selectedList}
                selectList={selectList}
                renameList={renameList}
                createList={() => setShowNewListModal(true)}
                deleteList={deleteList}
                addSharedUser={addSharedUser}
                deleteSharedUser={deleteSharedUser}
                setToastBody={setToastBody}
                logOut={logOut}
            />
            <div className='flex-grow-1  overflow-auto'>
                {
                    !displayedList() 
                        ? 
                            <Alert variant='info m-5'>
                                No hay listas!
                                <span className='ms-3' style={{cursor: 'pointer'}} onClick={() => setShowNewListModal(true)}>
                                    Crear lista
                                </span>
                            </Alert>
                        : <List 
                            list={displayedList()}
                            createListItem={createListItem}
                            editListItem={editListItem}
                            deleteListItem={deleteListItem}
                            setToastBody={setToastBody}
                            checkAll={checkAll}
                            clearList={clearList}
                        />
                }
            </div>
            <InputModal
                showModal={showNewListModal}
                closeModal={() => setShowNewListModal(false)}
                title='Nueva lista'
                action='Confirmar'
                handleSubmit={createList}
                inputs={[
                    {
                        name: 'nombre',
                        value: 'Nombre'
                    }
                ]}
            />
            <ToastAlert 
                toastBody={toastBody}
                toastBackground='warning'
                setToastBody={setToastBody}
            />
        </>
    );
};

export default App;
