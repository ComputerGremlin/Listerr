import { useState } from 'react';
import ListItem from './ListItem';
import {Button,	Container, Card, ListGroup,	InputGroup,	FormControl} 
	from 'react-bootstrap';
import InputModal from './InputModal';
import SimpleModal from './SimpleModal';

const List = (props) => {
    const [showRenameModal, setShowRenameModal] = useState(false);
    const [itemToRename, setItemToRename] = useState('');
    const [newListItemInputValue, setNewListItemInputValue] = useState('');
    const [showConfirmDeleteModal, setShowConfirmDeleteModal] = useState(false);

	const createListItem = (value, index) => {
		return (
			<ListItem
				value={value}
				key={index}
				index={index}
				onChildCheck={() => handleCheck(value)}
				deleteItem={() => deleteItem(value.id)}
				changeName={() => changeName(value)}
			/>
		);
	};

	const addListItem = (name) => {
		if (name && name.trim()) {
			props.createListItem({
				name: name,
				info: '',
				state: false,
				list: props.list.id
			});
		} else {
			props.setToastBody('Inserta un nombre válido');
		}
	};

	const handleCheck = (listItem) => {
		props.editListItem({
			id: listItem.id,
			name: listItem.text,
			info: listItem.description,
			state: !listItem.checked,
			list: props.list.id
		});
	};

	const handleConfirm = () => {
        setShowConfirmDeleteModal(false);
        props.clearList(props.list.id);
    };

	const clearList = () => {
        setShowConfirmDeleteModal(true);
	};

	const deleteItem = (id) => {
		props.deleteListItem(id);
	};
	
	const changeName = (item) => {
		setItemToRename(item);
		setShowRenameModal(true);
	};

	const handleRename = (inputValues) => {
		if (!inputValues.nombre) {
			props.setToastBody('Inserta un nombre válido');
            return;
        }
		props.editListItem({
			id: itemToRename.id,
			name: inputValues.nombre, 
			info: inputValues.descripcion, 
			state: itemToRename.checked,
			list: props.list.id
		});
	};
	
	const handleListItemInputChange = (event) => {
        setNewListItemInputValue(event.target.value);
    };

	const handleListItemSubmit = (event) => {
        event.preventDefault();
        addListItem(newListItemInputValue);
        setNewListItemInputValue('');
    };

	return (
		<Container className='p-5 m-auto border flex-nowrap'>
			<h1 className='text-center'>{props.list.name}</h1>
			<Card className='my-5'>
				<ListGroup variant='flush'>
					{props.list.list.map((value, index) => createListItem(value, index))}
					<ListGroup.Item>
						<form onSubmit={handleListItemSubmit}>
							<InputGroup>
								<FormControl 
									onChange={handleListItemInputChange}
									value={newListItemInputValue}
								/>
								<Button variant='outline-primary' type='submit'>
									Añadir
								</Button>
							</InputGroup>
						</form>
					</ListGroup.Item>
				</ListGroup>
			</Card>
			<Button className='m-1' variant='dark' onClick={() => props.checkAll(props.list.id)}>
				<i className='bi-check-all'></i> Checkea todo
			</Button>
			<Button className='m-1' variant='danger' onClick={clearList}>
				Vaciar lista
			</Button>
			<InputModal
                showModal={showRenameModal}
                closeModal={() => setShowRenameModal(false)}
                title='Modificar checkbox'
                action='Confirmar'
                handleSubmit={handleRename}
				inputs={[
					{
						name: 'nombre', 
						label: 'Nombre', 
						value: itemToRename.text
					}, 
					{
						name: 'descripcion', 
						label: 'Descripción', 
						as: 'textarea', 
						value: itemToRename.description
					}
				]}
            />
			<SimpleModal
                showModal={showConfirmDeleteModal}
                closeModal={() => setShowConfirmDeleteModal(false)}
                title='¿Seguro que quiere vaciar esta lista?'
                body='Esta acción no se puede deshacer.'
                action='Confirmar'
                handleConfirm={handleConfirm}
                buttonType='danger'
            />
		</Container>
	);
};

export default List;
