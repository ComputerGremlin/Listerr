import { Stack, Form, Button, OverlayTrigger, Tooltip, ListGroup } from 'react-bootstrap';

const ListItem = (props) => {

	const renderTooltip = (overlayProps, description) => {
		return (
			<Tooltip id='button-tooltip' {...overlayProps}>
				{description}
			</Tooltip>
		);
	};

	const createOverlay = (body, data) => {
		return (
			<OverlayTrigger
				placement='bottom'
				delay={{ show: 250, hide: 400 }}
				overlay={
					(overlayProps) => renderTooltip(overlayProps, data)
				}
			>
				<div>{body}</div>
			</OverlayTrigger>
		);
	};

	const itemStack = (
		<Stack direction='horizontal'>
			<div>
				<Form.Check 
					type='checkbox'
					label={props.value.text}
					checked={props.value.checked}
					onChange={props.onChildCheck}
				/>
			</div>
			<div className='ms-auto'>
				<Button 
					size='sm'
					variant='outline-primary'
					onClick={props.changeName}
				>
					<i className='bi-pencil-square'></i>
				</Button>
				<Button 
					className='m-1'
					size='sm'
					variant='outline-danger'
					onClick={props.deleteItem}
				>
					<i className='bi-trash3'></i>
				</Button>
			</div>
		</Stack>
	);

	return (
		<ListGroup.Item key={props.index} >
			{props.value.description ? createOverlay(itemStack, props.value.description) : itemStack}
		</ListGroup.Item>
	)
};

export default ListItem;
