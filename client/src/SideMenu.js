import { useState, useEffect } from 'react';
import SideMenuItem from './SideMenuItem';
import Button from 'react-bootstrap/Button';
import InputModal from './InputModal';
import SimpleModal from './SimpleModal';

const SideMenu = (props) => {
    const [showRenameModal, setShowRenameModal] = useState(false);
    const [showConfirmDeleteModal, setShowConfirmDeleteModal] = useState(false);
    const [listToEdit, setListToEdit] = useState('');
    const [listToDelete, setListToDelete] = useState('');
    const [myOwnLists, setMyOwnLists] = useState([]);
    const [mySharedLists, setMySharedLists] = useState([]);

    useEffect(() => {
        if (listToEdit != '') {
            props.myLists.forEach(list => {
                if (listToEdit.id === list.id) {
                    setListToEdit(list);
                }
            });
        }

        const auxMyOwnLists = [], 
            auxMySharedLists = [];
        for (var i in props.myLists) {
            if (props.myLists[i].ownerUserID == props.user) {
                auxMyOwnLists.push(props.myLists[i]);
            } else {
                auxMySharedLists.push(props.myLists[i]);
            }
        }
        setMyOwnLists(auxMyOwnLists);
        setMySharedLists(auxMySharedLists);
    }, [props.myLists]);

	const createSideMenuItem = (list, isShared) => {
        let sharedID;
        console.log(list);
        for (const sharedInstace of list.shared) {
            if (props.user === sharedInstace.userID) {
                sharedID = sharedInstace.id;
            }
        }
		return(
            <SideMenuItem
                number={list.id} 
                key={list.id}
                name={list.name}
                list={list.list}
                isShared={isShared}
                sharedID={sharedID}
                handleSharedUserDelete={handleSharedUserDelete}
                selectedList={props.selectedList}
                selectList={selectList}
                renameList={renameList}
                deleteList={deleteList}
            />
		);
	};

    const renameList = (id) => {
        props.myLists.forEach(list => {
            if (id === list.id) {
                setListToEdit(list);
                setShowRenameModal(true);
            }
        });
    };

    const handleSubmit = (inputValues) => {
        let newName = '';
        if (inputValues.nombre && inputValues.nombre.trim()) {
            newName = inputValues.nombre;
        } else {
            newName = listToEdit.name;
            props.setToastBody('Inserta un nombre válido');
        }
        props.renameList({
            id: listToEdit.id,
            name: newName
        });
    };

    const handleSharedUserSubmit = (username) => {
        props.addSharedUser({
            id: listToEdit.id,
            username: username
        });
    };

    const handleSharedUserDelete = (sharedUserID) => {
        props.deleteSharedUser(sharedUserID);
    };

    const handleConfirm = () => {
        setShowConfirmDeleteModal(false);
        props.deleteList(listToDelete)
    };

    const selectList = props.selectList;
    const deleteList = (id) => {
        setShowConfirmDeleteModal(true);
        setListToDelete(id);
    };

    return (
        <div className='d-flex flex-column flex-shrink-0 p-3 text-white bg-dark'>
            {
                myOwnLists && myOwnLists.length
                ?
                    <>
                        <span className='fs-4'>Mis listas</span>
                        <hr/>
                        <ul className='nav overflow-auto flex-nowrap nav-pills flex-column mb-5'>
                            {myOwnLists.map((list) => createSideMenuItem(list))} 
                        </ul>
                        </>
                : <></>
            }
            {
                mySharedLists && mySharedLists.length
                ?
                    <>
                        <span className='fs-4'>Compartidas conmigo</span>
                        <hr/>
                        <ul className='nav overflow-auto flex-nowrap nav-pills flex-column'>
                            {mySharedLists.map((list) => createSideMenuItem(list, true))} 
                        </ul>
                    </>
                : <></>
            }
            <hr className='mt-auto'/>
            <Button
                variant='dark' 
                onClick={props.createList}
            >
                Nueva lista
            </Button>
            <Button
                variant='dark' 
                onClick={props.logOut}
            >
                Log out
            </Button>
            <InputModal
                showModal={showRenameModal}
                closeModal={() => setShowRenameModal(false)}
                title='Editar'
                action='Confirmar'
                setToastBody={props.setToastBody}
                handleSubmit={handleSubmit}
                handleSharedUserSubmit={handleSharedUserSubmit}
                handleSharedUserDelete={handleSharedUserDelete}
                inputs={[
                    {
                        name: 'nombre',
						label: 'Nombre', 
                        value: listToEdit.name
                    }
                ]}
                userList={listToEdit.shared ? listToEdit.shared : []}
            />
            <SimpleModal
                showModal={showConfirmDeleteModal}
                closeModal={() => setShowConfirmDeleteModal(false)}
                title='¿Seguro que quiere eliminar esta lista?'
                body='Esta acción no se puede deshacer.'
                action='Confirmar'
                handleConfirm={handleConfirm}
                buttonType='danger'
            />
        </div>
    );
};

export default SideMenu;
