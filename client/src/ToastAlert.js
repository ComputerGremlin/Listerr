import { Toast, ToastContainer } from 'react-bootstrap';

const ToastAlert = (props) => (
    <ToastContainer className='p-3' position='top-end'>
        <Toast 
            onClose={() => props.setToastBody('')} show={props.toastBody !== ''} 
            bg={props.toastBackground}
            delay={3000} 
            autohide={true}
        >
            <Toast.Body>
                <i className='bi-exclamation-triangle'></i> {props.toastBody}
            </Toast.Body>
        </Toast>
    </ToastContainer>
);

export default ToastAlert;
