import { useState } from 'react';
import { Button, Card, ListGroup, InputGroup, FormControl, Stack, Modal, Form } from 'react-bootstrap';

const InputModal = (props) => {

    const [newSharedUserInputValue, setNewSharedUserInputValue] = useState('');

    let values = {};
    for (let i in props.inputs) {
        values[props.inputs[i].name] = '';
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        props.handleSubmit(values);
        props.closeModal();
    };

    const handleChange = (event) => {
        values[event.target.name] = event.target.value;
    };

    const handleSharedUserInputChange = (event) => {
        setNewSharedUserInputValue(event.target.value);
    }

    const handleSharedUserSubmit = (event) => {
        event.preventDefault();
        addSharedUser(newSharedUserInputValue);
        setNewSharedUserInputValue('');
    };

    const addSharedUser = (name) => {
		if (name && name.trim()) {
			props.handleSharedUserSubmit(name);
		} else {
			props.setToastBody('Inserta un nombre válido');
		}
	};

    const createFormInput = (item, index) => {
        return(
            <Form.Group 
                className={index !== 0 ? 'mt-2' : ''}
                key={index}
            >
                {item.label ? <Form.Label>{item.label}</Form.Label> : ''}
                
                <Form.Control 
                    name={item.name}
                    as={item.as}
                    rows='3'
                    onChange={handleChange}
                    placeholder={item.value}
                />
            </Form.Group>
		);
    };

    const createListItem = (value, index) => {
		return (
			<ListGroup.Item key={index}>
                <Stack direction='horizontal'>
                    <div>
                        {value.username}
                    </div>
                    <div className='ms-auto'>
                        <Button 
                            className='m-1'
                            size='sm'
                            variant='outline-danger'
                            onClick={() => props.handleSharedUserDelete(value.id)}
                        >
                            <i className='bi-trash3'></i>
                        </Button>
                    </div>
                </Stack>
            </ListGroup.Item>
		);
	};

    return (
        <Modal
            show={props.showModal}
            onHide={props.closeModal}
            backdrop='static'
        >
            <Modal.Header closeButton>
                <Modal.Title>{props.title}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form onSubmit={handleSubmit} id='myform'>
                    {props.inputs.map(
                        (item, index) => createFormInput(item, index)
                    )}
                </form>
                {
                    'userList' in props ?
                        <Card className='my-5'>
                            <Card.Body>
                                <Card.Title>Compartir lista</Card.Title>
                            </Card.Body>
                            <ListGroup variant='flush'>
                                {props.userList.map((value, index) => createListItem(value, index))}
                                
                                <ListGroup.Item>
                                    <form onSubmit={handleSharedUserSubmit}>
                                        <InputGroup>
                                            <FormControl 
                                                onChange={handleSharedUserInputChange}
                                                value={newSharedUserInputValue}
                                            />
                                            <Button variant='outline-primary' type='submit'>
                                                Añadir usuario
                                            </Button>
                                        </InputGroup>
                                    </form>
                                </ListGroup.Item>
                            </ListGroup>
                        </Card>
                    :
                        <></>
                }
            </Modal.Body>
            <Modal.Footer>
                <Button variant='secondary' onClick={props.closeModal}>
                    Cancelar
                </Button>
                <Button type='submit' form='myform' variant='primary'>{props.action}</Button>
            </Modal.Footer>
        </Modal>
    );
};

export default InputModal;
